<?php

namespace App\Http\Controllers;
use App\Models\User;
use App\Models\Post;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class StatController extends Controller
{

    public function index()
    {
        return view('statistic.show', [
            'recentUser' => User::latest()->take(3)->get(),
            'countComment' => DB::select("SELECT users.`id`, users.`email`, count(comments.user_id) as `commentCount` FROM users JOIN comments ON users.id=comments.user_id GROUP BY users.id ORDER BY commentCount DESC LIMIT 3"),
            'recentComment' => DB::select("SELECT posts.title, posts.id, comments.created_at, users.`name` as user FROM posts JOIN comments ON comments.post_id=posts.id JOIN users ON users.id=comments.user_id ORDER BY `comments`.created_at DESC LIMIT 3")
        ]);
    }

    //$recentUsers = DB::select('select `id`, `email`, `created_at` from users order by `created_at` DESC');
        //return view('statistic.show', [
        //    'recentUser' => $recentUsers
        //]);
}
