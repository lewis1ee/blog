<?php

namespace App\Http\Controllers;
use Illuminate\Validation\ValidationException;
use Illuminate\Http\Request;

class SessionsController extends Controller
{
    public function create()
    {
        return view('sessions.create');
    }

    public function store()
    {
        // validate the request
        $attributes = request()->validate([
            'username' => 'required',
            'password' => 'required'
        ]);

        // attempt to authenticate and login the user
        //based on the provided credentials
        if (! auth()->attempt($attributes)) 
        {
            throw ValidationException::withMessages([
                'username' => 'Your provided credentials could not be verified.'
            ]);
            
        }

        session()->regenerate();

        return redirect('/')->with('success', 'Welcome back!');
        //return back()
        //    ->withInput()
        //    ->withErrors(['username', 'Your provided credentials could not be verified.']);

        //redirect with a success flash message
    }

    public function destroy()
    {
        auth()->logout();

        return redirect('/')->with('success', 'Goodbye!');
    }
}
