<?php

namespace App\Http\Controllers;

use App\Models\Post;
use App\Models\User;
use App\Models\Category;
use Illuminate\Http\Request;

class PostCommentsController extends Controller
{
    //
    public function store(Post $post)
    {
        //validation
        request()->validate([
            'body' => 'required'
        ]);

        // add a comment to the given post
        $post->comments()->create([
            'user_id' => request()->user()->id,
            'body' => request('body')
        ]);

        return back();
    }
}
