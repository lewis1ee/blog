<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Comment;

class Post extends Model
{
    use HasFactory;


    //protected $guarded = ['id', 'timestamps'];
    //protected $fillable = ['title', 'excerpt', 'body'];
    
    protected $with = ['category', 'author'];

    public function scopeFilter($query, array $filters) //Post::newQuery()->filter_has_var
    {
        $query->when(isset($filters['search']) ? $filters['search'] : false, function ($query, $search) {
            $query->where(function ($query){
                $query->where('title', 'Like', '%' . request('search') . '%')
                ->orWhere('body', 'Like', '%' . request('search') . '%');
            });
        });

        $query->when(isset($filters['category']) ? $filters['category'] : false, function ($query, $category) {
            $query
                ->whereHas('category', function($query) use  ($category) {
                    $query
                        ->where('slug', $category);
            });
        });

        $query->when(isset($filters['author']) ? $filters['author'] : false, function ($query, $author) {
            $query
                ->whereHas('author', function($query) use  ($author) {
                    $query
                        ->where('username', $author);
            });
        });
    }

    public function getRouteKeyName()
    {
        return 'slug';
    }

    public function comments()
    {
        return $this->hasMany(Comment::class);
    }
    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    public function author()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
}
