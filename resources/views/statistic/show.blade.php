<!doctype html>
<main>
    <article>
        <div class="lg:text-center width:100%">
            <table width="100%">
                <tr>
                    <th width="33%">
                        User id
                    </th>
                    <th width="34%">
                        Email
                    </th>
                    <th width="33%">
                        Registered at
                    </th>
                @foreach ($recentUser as $user)
                    <tr>
                        <th> {{$user->id}} </th>
                        <th> {{$user->email}} </th>
                        <th> {{$user->created_at}} </th>
                    </tr>
                @endforeach
                </tr>

                <tr></tr>
                <tr></tr>
                <tr></tr>
                <tr></tr>
                <tr></tr>

                <tr>
                    <th>
                        User id
                    </th>
                    <th>
                        Email
                    </th>
                    <th>
                        Comment count
                    </th>
                    @foreach ($countComment as $count)
                        <tr>
                            <th>{{$count->id}}</th>
                            <th>{{$count->email}}</th>
                            <th>{{$count->commentCount}}</th>
                        </tr>
                    @endforeach
                </tr>

                <tr></tr>
                <tr></tr>
                <tr></tr>
                <tr></tr>
                <tr></tr>

                <tr>
                    <th>
                        Blog Title(ID)
                    </th>
                    <th>
                        Last Commented At
                    </th>
                    <th>
                        Last Commented By
                    </th>
                        @foreach ($recentComment as $comment)
                            <tr>
                                <th>{{$comment->title}}({{$comment->id}})</th>
                                <th>{{$comment->created_at}}</th>
                                <th>{{$comment->user}}</th>
                            </tr>
                        @endforeach
                </tr>
            </table>
        </div>
    </article>
</main>
