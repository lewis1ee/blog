<x-layout>
    <section class="px-6 py-8">
        <main class="max-w-lg mx-auto mt-10 bg-gray-50 border border-gary-150 p-6 rounded-xl">
        <h1 class="text-center font-bold text-xl">Login</h1>
            <form method="POST" action="/login" class="mt-10">

                @csrf                

                <div class="mb-8">
                    <label class="block mb-2 uppercase font-bold text-xs text-gray-700" for="username">
                        username
                    </label>

                    <input class="border border-gray-400 p-2 w-full" type="text" name="username" id="username" value="{{ old('username') }}" required>

                    @error('username')
                    <p class="text-red-500 text-xs mt-1">{{ $message }}</p>
                    @enderror
                </div>

                <div class="mb-8">
                    <label class="block mb-2 uppercase font-bold text-xs text-gray-700" for="password">
                        Password
                    </label>

                    <input class="border border-gray-400 p-2 w-full" type="password" name="password" id="password" required>

                    @error('password')
                    <p class="text-red-500 text-xs mt-1">{{ $message }}</p>
                    @enderror
                </div>

                <div class="mb-8">
                    <button type="submit" class="bg-gray-200 rounded py-2 px-4 hover:bg-gray-500 hover:text-white">
                        login
                    </button>
                </div>

            </form>
        </main> 
    </section>
</x-layout>