<button {{ $attributes(['class' => 'transition-colors duration-300 bg-blue-500 hover:bg-blue-600 ml-3 rounded-full font-bold text-white py-3 px-5']) }} >
    {{ $slot }}
</button>